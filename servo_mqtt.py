#!/usr/bin/python3

import paho.mqtt.client as mqtt
import pigpio
import time
import math
import struct
import sys

SERVO = 21
pi = pigpio.pi()
SERVER = 'iot.eclipse.org'
PORT = 1883
BASETOPIC = "nitk/demo/"
STATUS = 'status'
CONTROL = 'servo'

def saveTheValue(value=None):
    print("received a value {}".format(value))
    deg = math.ceil((value / 180) * 20)
    if deg == 0:
        deg = 1
    print("converted to {}".format(deg))
    if deg > 200:
        return
    pi.set_PWM_dutycycle(SERVO, deg)
    time.sleep(1)
    pi.set_PWM_dutycycle(SERVO, 0)
    return "done"

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    #print(msg.topic+" "+str(msg.payload))
    print("-> Received - " + msg.topic + ': ' + str(msg.payload))
    if msg.topic == (BASETOPIC + CONTROL):
        try:
            value = int.from_bytes(msg.payload, byteorder='big', signed=False)
        except TypeError:
            return
        saveTheValue(value)

if __name__ == '__main__':
    # 50Hz PWM
    pi.set_PWM_frequency(SERVO, 50)
    # 20ms mapped to range of 200
    pi.set_PWM_range(SERVO, 200)
    pi.set_PWM_dutycycle(SERVO, 10)

    print("MQTT server {}:{}".format(SERVER, PORT))
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
 
    client.connect(SERVER, PORT, 60)
    client.subscribe(BASETOPIC + CONTROL)
    client.publish(BASETOPIC + STATUS, payload="READY")
    client.loop_forever()
