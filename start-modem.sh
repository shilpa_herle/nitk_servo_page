#!/usr/bin/env bash

# Set GPIO to reset the modem board
echo Toggling GPIO17 to start the GPRS modem shield
echo 17 > /sys/class/gpio/export
sleep 2
echo out > /sys/class/gpio/gpio17/direction
sleep 2
gpio=`cat /sys/class/gpio/gpio17/value`
echo GPIO value is $gpio
if [ $gpio == "0" ];  then
  echo 1 > /sys/class/gpio/gpio17/value
  for i in `seq 0 10 60`; do
    echo "Waiting for modem to boot up ($i/60 s)..."
    sleep 10
  done
fi
sleep 2

# Startup pigpiod
echo Starting pigpiod
sudo pigpiod

# Start GPRS connection
echo Trying to connect to GPRS network
echo NOTE: Change the APN in /etc/ppp/peers/gprs, set to airtelgprs.com as of now
sudo pon gprs
sleep 2

echo Checking ifconfig
ifconfig ppp0

# Start server
echo Start MQTT client to drive server
/home/pi/servoserv/servo_mqtt.py
